<?php

namespace APP\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class City extends DB{
    private $id;
    private $userName;
    private $city;

    public function setData($allPostData = null)
    {
        if(array_key_exists("id",$allPostData)){
            $this->id = $allPostData['id'];
        }
        if(array_key_exists("userName",$allPostData)){
            $this->userName = $allPostData['userName'];
        }
        if(array_key_exists("city",$allPostData)){
            $this->city = $allPostData['city'];
        }
    }

    public function store()
    {
        $arrayData = array($this->userName,$this->city);
        $query = 'INSERT INTO city (username, cityName) VALUES (?,?)';
        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been stored!.");
        }
        else{
            Message::setMessage("Failed! Data has been stored!.");
        }

        Utility::redirect('index.php');
    }
    public function index(){

        $sql= "select * from city where soft_delet='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function view(){

        $sql = "select * from city where id=" . $this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function trashed(){

        $sql= "select * from city where soft_delet='No'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
}