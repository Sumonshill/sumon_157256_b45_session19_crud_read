<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Birth Date Collection Form</title>
    <link rel="stylesheet" href="../../../src/BITM/SEIP_157256/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.min.css">
</head>
<body>
    <div class="main-body">
        <div class="header">
            <h1 href="#">Birth Date Collection Form</h1>
        </div>
        <div class="notification">
            <div class="message text-center">
                <h3><?php echo $msg;?></h3>
            </div>
        </div>
        <form action="store.php" method="post" class="text-center">
            <div class="form-group">
                <label for="userName">Enter User Name</label>
                <input type="text" class="form-control text-center" name="userName" placeholder="Type User Name Here">
            </div>
            <div class="form-group">
                <label for="birthDate">Enter Date of Birth</label>
                <input type="date" class="form-control text-center" name="birthDate" placeholder="Select Your Birth Date">
            </div>
            <button type="submit" class="btn btn-lg btn-primary">Submit</button>
        </form>
    </div>


</body>
</html>
