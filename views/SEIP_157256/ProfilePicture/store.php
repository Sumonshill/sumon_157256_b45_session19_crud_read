<?php
require_once("../../../vendor/autoload.php");

use \App\ProfilePicture\ProfilePicture;

$objProfilePicture = new ProfilePicture();

$objProfilePicture->setData($_POST);

$objProfilePicture->setProfilePicture($_FILES);

$objProfilePicture->storePicture();

$objProfilePicture->store();