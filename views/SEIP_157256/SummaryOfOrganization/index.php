<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

$obj2 = new \App\SummaryOfOrganization\SummaryOfOrganization();
$allData= $obj2->index();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../../src/BITM/SEIP_157256/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.min.css">
</head>
<body>
  <div class="container ">
      <h2 class="text-center btn-info">SUMMARY OF ORGANIZATION ACTIVE LIST INFORMATION</h2>
   <table class="table table -striped">
       <th>SERIAL NUMBER</th>
       <th>ID</th>
       <th>USER NAME</th>
       <th>DESCRIPTION</th>
       <th>Action</th>

       <?php
       $serialnumber=1;
       foreach($allData as $oneData){

           echo "
               <tr>
                   <td>$serialnumber</td>
                   <td>$oneData->id</td>
                   <td>$oneData->name</td>
                   <td>$oneData->description</td>
                   
                   <td><a href='view.php?id=$oneData->id'>VIEW</a> </td>


                </tr>
           ";
       $serialnumber++;
       }
       ?>


   </table>
      </div>



<script src="../../../resource/bootstrap-3.3.7/js/jquery.min.js"></script>
<script src="../../../resource/bootstrap-3.3.7/js/bootstrap.min.js"></script>
<script>
    jQuery(function($){
        $('.message').fadeOut(550);
        $('.message').fadeIn(550);
        $('.message').fadeOut(550);
        $('.message').fadeIn(550);
        $('.message').fadeOut(550);
        $('.message').fadeIn(550);
        $('.message').fadeOut(550);
    })
</script>
</body>
</html>
