<?php
require_once ("../../../vendor/autoload.php");
$obj2 = new \App\SummaryOfOrganization\SummaryOfOrganization();
$obj2->setData($_GET);
$oneData= $obj2->view();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../../../src/BITM/SEIP_157256/Stylesheet/style.css">
    <link rel="stylesheet" href="../../../resource/bootstrap-3.3.7/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<table class="table table-striped">
   <?php echo "
    <tr>
        <td>Id :</td>
        <td>$oneData->id</td>
    </tr>
    <tr>
        <td>Name :</td>
        <td>$oneData->name</td>
    </tr>
    <tr>
        <td>DESCRIPTION :</td>
        <td>$oneData->description</td>
    </tr>
    ";
?>
    
</table>
</div>

</body>
</html>
